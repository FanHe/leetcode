/**
 * @author FanHe
 * 位运算问题
 * 通过异或运算找出出现一次的值
 */
public class Solution {
    public int singleNumber(int[] A) {
        if(A.length<=1){
            return A[0];
        }
        int res = A[0]^A[1];
        for(int i=2;i<A.length;i++){
            res = res^A[i];
        }
        
        return res;
    }
}