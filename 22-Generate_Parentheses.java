/**
 * @author FanHe
 * 想到二叉树==》想到递归
 * 所有可以化为二叉树的问题都是递归
 * 注意判断结束条件
 * 本题还有一个解法就是找规律，每次下一层比上一层多出的规律
 */ 
public class Solution {
    List<String> res = new ArrayList<String>();
    public List<String> generateParenthesis(int n) {
        tree("(",1,1,n);
        return res;
    }
    public void tree(String s,int depth,int leftCount,int n){
        //左括号个数大于了总的括号数
        if(leftCount > n){
            return;
        }
        //右括号数大于左括号数
        if(depth> 2*leftCount){
            return;
        }
        //达成条件
        if(depth == 2*n){
            res.add(s.toString());
            return;
        }
        tree(s+"(",depth+1,leftCount+1,n);
        tree(s+")",depth+1,leftCount,n);
    }
}