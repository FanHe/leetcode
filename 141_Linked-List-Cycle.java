/**
 * @author FanHe
 * 设置两个指针，一个步进2，一个步进1，最后能相遇，就是存在环
 */
/**
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public boolean hasCycle(ListNode head) {
        if(head == null || head.next == null){
            return false;
        }
        if(head.next == head){
            return true;
        }
        ListNode slow = head;
        ListNode fast = head;
        boolean res = false;
        while(true){
            if(fast.next!=null && fast.next.next!=null){
                fast = fast.next.next;
            }else{
                return false;
            }
            slow = slow.next;
            if(slow == fast){
                res = true;
                break;
            }
        }
        return res;
    }
}