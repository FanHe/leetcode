/**
 * @author FanHe
 * 二分法查找插入位置
 */
public class Solution {
    public int searchInsert(int[] A, int target) {
        int size = A.length;
        int left = 0;
        int right = size;
        int mid = (right+left)/2;
        while(mid != left && mid != right){
            if(A[mid] == target){
                return mid;
            }

            if(A[mid] > target){
                right = mid;
                mid = (right+left)/2;
            }else{
                left = mid;
                mid = (right+left)/2;
            }
            
        }
        if(A[mid]<target){
            return mid+1;
        }else{
            return mid;
        }
    }
}