import java.util.Vector;
public class Solution {
    public int numTrees(int n) {
    	Vector<Integer> num = new Vector<Integer>();
        num.add(1);
        for(int i=1; i<=n; i++){  
            num.add(0);  
            if(i<3)  
                num.set(i, i);
            else{  
                for(int j=1; j<=i; j++)  
                	num.set(i, num.get(i)+num.get(j-1)*num.get(i-j));
            }  
        }  
        return num.get(n);  
    }
}