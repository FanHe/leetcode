/**
 * @author FanHe
 * 挑选第一个数小的链，将剩下的链直接插入进去
 * 注意点：在分配next指针的，注意保存临时变量
 */ 
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        //ll1为较小的开头
        ListNode ll1,ll2;
        if(l1 == null){
            return l2;
        }
        if(l2 == null){
            return l1;
        }
        if(l1.val < l2.val){
            ll1 = l1;
            ll2 = l2;
        }else{
            ll1 = l2;
            ll2 = l1;
        }
        //指向当前操作对象
        ListNode p_ll1 = ll1;
        ListNode p_ll2 = ll2;
        while(p_ll2!=null){
            if(p_ll1.next == null){
                p_ll1.next = p_ll2;
                ListNode temp = p_ll2;
                p_ll2 = p_ll2.next;
                temp.next = null;
                p_ll1 = p_ll1.next;
                continue;
            }
            //寻找合适的插入位置
            while(p_ll1.next != null && !(p_ll2.val>=p_ll1.val&&p_ll2.val<=p_ll1.next.val)){
                p_ll1 = p_ll1.next; 

            }
            //这里注意要保存临时变量
            ListNode p_ll2next = p_ll2.next;
            p_ll2.next = p_ll1.next;
            p_ll1.next = p_ll2;
            p_ll2 = p_ll2next;               

        }
        return ll1;
    }
}