/**
 * @author FanHe
 * 本题类似于微软面试题，找出水王
 * 如果按照排序来，复杂度至少n*logN
 * 可以一次删两个不同的来进行，复杂度N
 * nTimes 累计出现次数
 * 当累计出现次数等于0了，相当于当前的candidate消除完了，选择一个新的candidate
 */
public class Solution {
    public int majorityElement(int[] num) {
        int candidate = 0;
        int nTimes,i;
        for(i=0,nTimes=0;i<num.length;i++){
            if(nTimes == 0){
                candidate = num[i];
                nTimes = 1;
            }else{
                if(candidate == num[i])
                    nTimes++;
                else
                    nTimes--;
            }
        }
        return candidate;
    }
}