/**
 * @author FanHe
 * 二分法搜索解决
 * 难点：判断最小元素存在左还是右
 */ 
public class Solution {
    public int findMin(int[] num) {
        return findMin2(num,0,num.length-1);
    }
    public int findMin2(int[] num,int left, int right){
        int mid = (right+left)/2;
        if(left == right){
            return num[left];
        }
        if(right-left == 1){
            return num[left]>num[right]?num[right]:num[left];
        }
        //最小在左
        if(num[mid]<num[left] && num[mid]<num[right]){
            //判断自己是不是最小
            if(num[mid]<num[mid]-1){
                return num[mid];
            }
            right = mid;
        }else if(num[mid]>num[left] && num[mid]>num[right]){
            left = mid;
        }else if(num[mid]>num[left] && num[mid]<num[right]){
            right = mid;
        }else if(num[mid]<num[left] && num[mid]>num[right]){
            left = mid;
        }
        return findMin2(num,left,right);
    }
}