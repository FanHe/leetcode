/**
 * @author FanHe
 * 理解题意：替换所有elem的元素，返回长度
 * 题目里有个说明，就是超出了新长度之后的所有元素，不用管了
 */ 
public class Solution {
    public int removeElement(int[] A, int elem) {
        int pointer = 0;
        for(int i=0;i<A.length;i++){
            if(A[i] != elem){
                A[pointer++] = A[i];
            }
        }
        return pointer;
    }
}