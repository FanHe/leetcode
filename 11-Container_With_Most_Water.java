/**
 * @author FanHe
 * 当宽度越来越小，高度只有越来越高才能取得更大值
 */ 
public class Solution {
    public int maxArea(int[] height) {
        int res = 0;
        int l = 0;
        int r = height.length-1;
        while(l<r){
            res = Math.max(res,(r-l)*Math.min(height[l],height[r]));
            if(height[l]<height[r]){
                int k = l;
                while(k<r && height[k]<=height[l]){
                    k++;
                }
                l = k;
            }else{
                int k = r;
                while(k>l && height[k]<=height[r]){
                    k--;
                }
                r = k;
            }
        }
        return res;
    }
}