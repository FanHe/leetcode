/**
 * @author FanHe
 * 本题的两种解法
 * 第一种是桶排序
 * 第二种是两个指针前后互换
 */ 
public class Solution {
    public void sortColors(int[] A) {
        if(A==null || A.length == 0){
            return;
        }
       int left = 0;
       int right = A.length-1;
       int cur = left;
       while(cur<=right){
           if(A[cur] == 0){
               swap(A,left++,cur);
               cur=cur<left?left:cur;
           }else if(A[cur] == 2){
               swap(A,right--,cur);
           }else{
               cur++;
           }
       }
    }
    public void swap(int[] A,int i,int cur){
        int temp = A[i];  
        A[i] = A[cur];  
        A[cur] = temp;
    }
}


/**
 * Solution 1

public class Solution {
    public void sortColors(int[] A) {
        int[] bucket = new int[3];
        for(int i=0;i<A.length;i++){
            bucket[A[i]]++;
        }
        int z = 0;
        for(int i=0;i<3;i++){
            for(int j=0;j<bucket[i];j++){
                A[z++] = i;
            }
        }
    }
}
 */ 