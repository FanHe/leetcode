/**
 * 
 * @author HeFan
 * 经典回溯法问题
 * https://oj.leetcode.com/problems/n-queens/
 *
 */
public class Solution {
    List<String[]> res = new ArrayList<String[]>();
    public List<String[]> solveNQueens(int n) {
        int[] a = new int[n];
        for(int i=0;i<n;i++){
        	a[i] = -1;
        }
        q(0,n,a);
        return res;
    }
    /**
     * 遍历代码
     */
    public void q(int row,int n,int[] a){
        //对row行的位置遍历
        for(int i=0;i<n;i++){
            //如果可以可以放在i的位置上
            if(isOK(row,i,a,n)){
                a[row] = i;
                //找到了最优解，输出并回溯
                if(row==n-1){
                    printres(a,n);
                    a[row] = -1;
                    return;
                }
                q(row+1,n,a);
                //回溯
                a[row] = -1;
            }
        }
    }
    /**
     * 判断能否在row行的i位置放下棋子
     */
    public boolean isOK(int row,int i,int[] a,int n){
        //row
        for(int z=0;z<n;z++){
            //行判断
            if(a[z]==i){
                return false;
            }
            //对角线判断
            if(Math.abs(a[z]-i)==Math.abs(z-row) &&a[z]!=-1 && row != z){
                return false;
            }

        }
        return true;
    }
    /**
     * 输出
     */
    public void printres(int[] a,int n){
        String[] str = new String[n];
        for(int i=0;i<n;i++){
        	str[i] = "";
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(a[i]==j){
                    str[i] += "Q";
                }else{
                    str[i] += ".";
                }
            }
        }
        res.add(str);
    }
}