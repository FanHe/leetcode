/**
 * @author FanHe
 * http://blog.csdn.net/tuantuanls/article/details/8717262
 */ 
public class Solution {
    List<List<Integer>> res = new ArrayList<List<Integer>>();
    int N = 0;
    public List<List<Integer>> permute(int[] num) {
        N = num.length;
        per(num,0);
        return res;
    }
    public void per(int[] num,int i){
        if(i==N){
            List<Integer> l = new ArrayList<Integer>();
            for(int numm : num){
                l.add(numm);
            }
            res.add(l);
            return;
        }
        for(int j=i;j<N;j++){
            int temp = num[i];
            num[i] = num[j];
            num[j] = temp;
            per(num,i+1);
            num[j] = num[i];
            num[i] = temp;
        }
    }
}