/**
 * @author Fanhe
 * 判断两棵二叉树是否一致
 * 递归算法
 */
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public boolean isSameTree(TreeNode p, TreeNode q) {
        return panduan(p,q);
    }
    public boolean panduan(TreeNode p, TreeNode q) {
        if(p==null && q==null){
            return true;
        }else if(p!=null && q==null){
            return false;
        }else if(q!=null && p==null){
            return false;
        }
        if(p.val == q.val){
            boolean left = panduan(p.left,q.left);
            boolean right = panduan(p.right,q.right);
            return left&&right;
        }else{
            return false;
        }
    }
}