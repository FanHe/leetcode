/**
 * @author FanHe
 * 链表去重问题
 */ 

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode deleteDuplicates(ListNode head) {
        ListNode res = head;
        ListNode temp = head;
        while(temp!=null && temp.next != null){
            //如果重复了，就将指针引入下一个结点
            if(temp.val == temp.next.val){
                temp.next = temp.next.next;
            }else{
                //否则直接检查下一个
                 temp = temp.next;
            }
           
        }
        return res;
    }
}