/**
 * @author FanHe
 * 关键点：递归每个节点的高度，然后判断是否是平衡的
 */ 
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    boolean flag = true;
    public boolean isBalanced(TreeNode root) {
        height(root);
        return flag;
    }
    public int height(TreeNode node){
        //如果已经不平衡了，没有必要继续往下算
        if(!flag){
            return 0;
        }
        //叶子节点，高度为0
        if(node == null){
            return 0;
        }
        //左右子树的高度，每一层高度+1
        int hl = height(node.left)+1;
        int hr = height(node.right)+1;
        //判断是否平衡
        if(Math.abs(hr-hl)>1){
            flag = false;
        }
        return hl>hr?hl:hr;
    }
}