/**
 * @author FanHe
 * int 数据共有32位，可以用32变量存储 这 N 个元素中各个二进制位上  1  出现的次数
 * 最后 在进行 模三 操作，如果为1，那说明这一位是要找元素二进制表示中为 1 的那一位。
 */ 
public class Solution {
    public int singleNumber(int[] A) {
        //分别计算32位上每一位出现的次数
        int[] bitnumber = new int[32];
        int result = 0;
        
        //找每一位上1出现的次数
        for(int i=0;i<32;i++){
            for(int j=0;j<A.length;j++){
                //第i位上出现1的次数
                bitnumber[i] += A[j]>>i&1;
            }
            //如果%3==1，说明这一位是要找的那个数的二进制表达中出现1的那一位
            result += (bitnumber[i]%3)<<i;
        }
        
        return result;
    }
}