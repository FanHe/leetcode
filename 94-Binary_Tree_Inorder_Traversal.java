/**
 * @author FanHe
 * 中序遍历的非递归实现
 */
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> r = new ArrayList<Integer>();
        Stack<TreeNode> s = new Stack<TreeNode>();
        if(root == null){
            return r;
        }
        while(root!=null || !s.empty()){
            while(root != null){
                s.push(root);
                root = root.left;
            }
            if(!s.empty()){
                root = s.pop();
                r.add(root.val);
                root = root.right;
            }
        }
        return r;
    }
}