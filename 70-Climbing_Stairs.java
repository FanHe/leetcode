/**
 * @author FanHe
 * 动态规划问题
 * DP[i]到第i个台阶时已有多少种
 * DP[n] = DP[n-1]+DP[n-2]
 * 这里可以使用一个缓存，来保存已经计算过的值
 */
public class Solution {
    Map<Integer,Integer> map = new HashMap<Integer,Integer>();
    public int dp(int n){

        if(!map.containsKey(n-1)){
            map.put(n-1,climbStairs(n-1));
        }
        return map.get(n-1)+map.get(n-2);
    }
    public int climbStairs(int n) {
        if(n==1){
            return 1;
        }
        if(n==2){
            return 2;
        }
        map.put(1,1);
        map.put(2,2);
        return dp(n);
    }
}