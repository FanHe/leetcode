/**
 * @author FanHe
 * 其实就是个26进制计算问题
 */
public class Solution {
    public int titleToNumber(String s) {
        int res = 0;
    	char[] charArray = s.toCharArray();
    	int wei = 0;
        for(int i=charArray.length-1;i>=0;i--){
        	int sub = charArray[i]-'A'+1;
        	res += sub*Math.pow(26, wei++);
        }
    	return res;
    }
}