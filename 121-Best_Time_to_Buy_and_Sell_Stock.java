/**
 * @author FanHe
 * 记录最低值
 * 比较当前值-最低值与最大利润
 */ 
public class Solution {
    public int maxProfit(int[] prices) {
        if(prices.length==0){
            return 0;
        }
        int lowprice = prices[0];
        int maxprofit = 0;
        for(int i=1;i<prices.length;i++){
            if(prices[i]<lowprice){
                lowprice = prices[i];
                continue;
            }
            if(prices[i]-lowprice>maxprofit){
                maxprofit = prices[i]-lowprice;
            }
        }
        return maxprofit;
    }
}