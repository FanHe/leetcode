/**
 * @author FanHe
 * 注意链接之间的关系
 */ 
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode swapPairs(ListNode head) {
        ListNode firstHead = head;
        if(head == null || head.next == null){
            return head;
        }
        ListNode tempNode = head.next;
        head.next = head.next.next;
        tempNode.next = head;
        head = tempNode;
        firstHead = tempNode;
        while(head.next.next!= null && head.next.next.next != null){
            ListNode beforeHead = head.next;
            head = head.next.next;
            tempNode = head.next;
            head.next = head.next.next;
            beforeHead.next = tempNode;
            tempNode.next = head;
            head = tempNode;
        }
        return firstHead;
    }
}