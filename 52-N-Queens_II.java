/**
 * @author FanHe
 * N-Queen问题的简单化
 * 只要输出多少种可能就OK
 */
public class Solution {
    public int totalNumber = 0;
    public int totalNQueens(int n) {
        int[] a = new int[n];
        for(int i=0;i<n;i++){
        	a[i] = -1;
        }
        q(0,n,a);
        return totalNumber;
    }
    public void q(int row,int n,int[] a){
        //对row行的位置遍历
        for(int i=0;i<n;i++){
            //如果可以可以放在i的位置上
            if(isOK(row,i,a,n)){
                a[row] = i;
                //找到了最优解，输出并回溯
                if(row==n-1){
                    totalNumber++;
                    a[row] = -1;
                    return;
                }
                q(row+1,n,a);
                //回溯
                a[row] = -1;
            }
        }
    }
    public boolean isOK(int row,int i,int[] a,int n){
        //row
        for(int z=0;z<n;z++){
            //行判断
            if(a[z]==i){
                return false;
            }
            //对角线判断
            if(Math.abs(a[z]-i)==Math.abs(z-row) &&a[z]!=-1 && row != z){
                return false;
            }

        }
        return true;
    }
}