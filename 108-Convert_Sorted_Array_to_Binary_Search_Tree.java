/**
 * @author FanHe
 * 有序列表建立二叉搜索树
 * 采用递归，每次取列表的中间作为根节点
 */ 
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public TreeNode sortedArrayToBST(int[] num) {
        return findRoot(num,0,num.length-1);
    }
    public TreeNode findRoot(int[] num,int left,int right){
        //已经没有孩子节点的情况
        if(left > right){
            return null;
        }
        //取中间值作为根节点
        int mid = (right+left)/2;
        TreeNode root = new TreeNode(num[mid]);
        //没有孩子节点的情况
        if(left==right){
            return root;
        }
        //将剩下列表的左右分别找子节点
        root.left = findRoot(num,left,mid-1);
        root.right = findRoot(num,mid+1,right);
        return root;
    }
}