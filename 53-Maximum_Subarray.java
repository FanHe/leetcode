/**
 * @author FanHe
 * 动态规划
 * maxValue[i]第i个时达到的最大Value
 * maxStart[i]第i个时的起始位置
 */ 
public class Solution {
    public int maxSubArray(int[] A) {
        int[] maxValue = new int[A.length];
        int[] maxStart = new int[A.length];
        maxValue[0] = A[0];
        maxStart[0] = 0;
        for(int i=1;i<A.length;i++){
            //DP
            if(A[i] > A[i]+maxValue[i-1]){
                maxStart[i] = i;
                maxValue[i] = A[i];
            }else{
                maxStart[i] = maxStart[i-1];
                maxValue[i] = A[i] + maxValue[i-1];
            }
        }
        int maxRes = maxValue[0];
        for(int i=1;i<maxValue.length;i++){
            if(maxValue[i]>maxRes){
                maxRes = maxValue[i];
            }
        }
        return maxRes;
    }
}