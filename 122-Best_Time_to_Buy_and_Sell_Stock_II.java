/**
 * @author FanHe
 * 平滑坡度问题
 * 注意，这里的一个小陷阱，就是只要是上升的就是利润，不管何时买卖都一样的
 */
public class Solution {
    public int maxProfit(int[] prices) {
        if(prices.length < 2){
            return 0;
        }
        int res=0;
        for(int i=0;i<prices.length-1;i++){
            if(prices[i]<prices[i+1]){
                res += prices[i+1]-prices[i];
            }
        }
        return res;
    }
}