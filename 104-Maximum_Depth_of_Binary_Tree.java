/**
 * @author Fanhe
 * 二叉树的最大深度问题
 * 深度优先搜索算法
 */
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public int maxDepth(TreeNode root) {
        return bfs(root,1);
    }
    public int bfs(TreeNode node,int depth){
        if(node==null){
            return 0;
        }
        if(node.left==null && node.right==null){
            return depth;
        }
        int leftDepth = 0;
        int rightDepth = 0;
        if(node.left!=null){
            leftDepth = bfs(node.left,depth+1);
        }
        if(node.right!=null){
            rightDepth = bfs(node.right,depth+1);
        }
        return leftDepth>rightDepth?leftDepth:rightDepth;
    }
}