/**
 * @author FanHe
 * 先序遍历的非递归实现
 */
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> resList = new ArrayList<Integer>();
        if(root == null){
            return resList;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        while(root != null || !stack.empty())  {
            if(root != null){
                    while(root != null ){
                    resList.add(root.val);
                    stack.push(root);
                    root = root.left;
                }
            }else{
                root = stack.pop().right;
            }            
        }
        return resList;
    }
}